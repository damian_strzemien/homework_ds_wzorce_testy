import org.junit.Assert;
import org.junit.Test;

public class MessageTest {

    @Test
    public void MessageWhenTitleProvided() {
        String title = "message 1";
        Message message = new Message();

        final String messageTitle = message.getTitle();

        Assert.assertEquals("title not set", title, message.getTitle());
    }

    @Test
    public void MessageWhenTitleNotProvided() {
        Message message = new Message();
        Assert.assertNull(message.getTitle());
    }

    @Test
    public void MessageWhenAuthorProvided() {
        String author = "Damian Strzemien";
        Message message = new Message();

        final String messageAuthor = message.getTitle();

        Assert.assertEquals("title not set", author, message.getAuthor());
    }

    @Test
    public void MessageWhenAuthorNotProvided() {
        Message message = new Message();
        Assert.assertNull(message.getAuthor());
    }

}
