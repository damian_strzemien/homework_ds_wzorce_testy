import java.util.List;
import java.util.stream.Collectors;

public class SimpleRepository {
    private SimpleMessageForm storage;

    public SimpleRepository(SimpleMessageForm storage) {
        this.storage = storage;
    }

    void add(Message message){
        storage.add(message);
    }

    void delete(Message message) {
        if(storage.getAll() != null){
            storage.delete(message);
        }
    }

    List<Message> find(MessageType messageType) {
        return storage.getAll().stream().filter(message -> message.getType()==messageType).collect(Collectors.toList());
    }


}
