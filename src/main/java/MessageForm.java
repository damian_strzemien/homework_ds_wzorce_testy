import java.util.List;

public interface MessageForm {
    void add(Message message);

    void delete(Message message);

    List<Message> getAll();


}
