import java.util.ArrayList;
import java.util.List;

public class SimpleMessageForm implements MessageForm {
    private List<Message> dataStorage = new ArrayList<>();

    @Override
    public void add (Message message) {
        if(message != null) {
            dataStorage.add(message);
        }

    }

    @Override
    public void delete (Message message) {
        if(message != null) {
            dataStorage.remove(message);
        }

    }

    @Override
    public List<Message> getAll() {
        List<Message> messages = new ArrayList<>();
        messages.addAll(dataStorage);
        return messages;
    }
}
