import java.util.Date;
import java.util.List;

public interface MessageRepository {

    void add(Message message);

    void delete(Message message);

    List<Message> getAll();
    List<Message> findByTitle(String title);
    List<Message> findByAuthor(String author);
    List<Message> findByDate(Date creationDate);
    List<Message> findByType(MessageType messageType);
}
