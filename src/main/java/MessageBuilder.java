import java.util.Date;

public class MessageBuilder {
    String title;
    String author;
    String text;
    Date creationDate;
    MessageType type;



    public MessageBuilder withTitle(String title) {
        this.title = title;
        return this;
    }

    public MessageBuilder withAuthor(String author) {
        this.author = author;
        return this;
    }

    public MessageBuilder withText(String text) {
        this.text = text;
        return this;
    }

    public MessageBuilder withDate (Date date) {
        this.creationDate = date;
        return this;
    }

    public MessageBuilder withType (MessageType type) {
        this.type = type;
        return this;
    }

    public Message build() {
        Message m = new Message();
        m.setTitle(this.title);
        m.setAuthor(this.author);
        m.setText(this.text);
        m.setCreationDate(this.creationDate);
        m.setType(this.type);
        return m;
    }
}
