import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class SimpleRepToMessageRepAdapter implements MessageRepository {
    private final SimpleRepository simpleRepository;

    public SimpleRepToMessageRepAdapter(SimpleRepository simpleRepository) {
        this.simpleRepository = simpleRepository;
    }

    @Override
    public void add(Message message) {
        if(message != null){
            simpleRepository.add(message);
        }
    }

    @Override
    public void delete(Message message) {
        if(message != null){
            simpleRepository.delete(message);
        }
    }

    @Override
    public List<Message> getAll() {
        List<Message> messages = new ArrayList<>();
        messages.addAll((Collection<? extends Message>) simpleRepository);
        return null;

    }

    @Override
    public List<Message> findByTitle (String title) {
        List<Message> messages = new ArrayList<>();
        messages.stream().map(Message::getTitle).collect(Collectors.toList());
        return findByTitle(title);
/*
sorted().collect(Collectors.toList());
stream().map(Message::getTitle).collect(Collectors.toList());
stream().map(Message::getTitle).forEach(System.out::println);
stream().filter(m -> m.getTitle().equals(title));
*/
    }

    @Override
    public List<Message> findByAuthor(String author) {
        List<Message> messages = new ArrayList<>();
        messages.stream().map(Message::getAuthor).collect(Collectors.toList());
        return findByAuthor(author);
    }

    @Override
    public List<Message> findByDate (Date creationDate){
        List<Message> messages = new ArrayList<>();
        messages.stream().map(Message::getCreationDate).collect(Collectors.toList());
        return findByDate(creationDate);
    }

    @Override
    public List<Message> findByType(MessageType messageType) {
        List<Message> messages = new ArrayList<>();
        messages.stream().map(Message::getType).collect(Collectors.toList());
        return findByType(messageType);
    }
}
